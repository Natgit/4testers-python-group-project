from src.oop.cars import Car


def test_new_car():
    test_car = Car("model", 2022)
    assert test_car.mileage == 0


def test_total_distance_drive_100():
    test_car = Car("model", 2022)
    test_car.drive(100)
    assert test_car.mileage == 100


def test_total_distance_drive_100_and_200():
    test_car = Car("model", 2022)
    test_car.drive(100)
    test_car.drive(200)
    assert test_car.mileage == 300


def test_has_warranty_if_valid():
    test_car = Car("model", 2020)
    test_car.drive(119_000)
    assert test_car.has_warranty() is True


def test_the_warranty_if_invalid():
    test_car = Car("model", 2007)
    test_car.drive(19_000)
    assert test_car.has_warranty() is False


def test_the_description():
    test_car = Car("Honda Civic", 1998)
    test_car.drive(230_000)
    assert test_car.get_description() == "This is a Honda Civic made in 1998. Currently it drove 230000 kilometers."
