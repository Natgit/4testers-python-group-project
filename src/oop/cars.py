import datetime


class Car:

    def __init__(self, model, manufacture_year):
        self.model = model
        self.manufacture_year = manufacture_year
        self.mileage = 0

    # drive(distance) służąca do jazdy (zwiększa przebieg samochodu o podany dystans), zwraca None
    def drive(self, total_distance):
        self.mileage += total_distance

    # has_warranty() - czy auto ma gwarancję. Zwraca False jeśli wiek samochodu jest większy niż 7 lat lub przebieg większy niż 120 tys.
    def has_warranty(self):
        date = datetime.date.today()
        year = date.year
        age_of_car = year - self.manufacture_year
        return False if age_of_car > 7 or self.mileage > 120_000 else True

    # get_description() - zwraca string opisujący auto:
    # “This is a MODEL made in MANUFACTURE_YEAR. Currently it drove TOTAL_DISTANCE kilometers”
    def get_description(self):
        return f'This is a {self.model} made in {self.manufacture_year}. Currently it drove {self.mileage} kilometers.'

if __name__ == '__main__':
    car1 = Car('Toyota', 2020)
    car2 = Car('VW', 2000)
# przejedź nimi trochę kilometrów
    car1.drive(300)
    car2.drive(500)

# Sprawdź czy samochody mają gwarancję
    print(car1.has_warranty())
    print(car2.has_warranty())
# Wydrukuj ich opisy
    print(car1.get_description())
    print(car2.get_description())

